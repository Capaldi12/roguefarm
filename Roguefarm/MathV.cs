﻿using System;
using SFML.System;

namespace Roguefarm
{
    //Class for some vector math
    class MathV
    {
        public static readonly Vector2f Zero = new Vector2f(0, 0);

        //Dot product of two vectors
        public static float Dot(Vector2f left, Vector2f right)
        {
            return left.X * right.X + left.Y * right.Y;
        }

        //Length of vector
        public static float Length(Vector2f v)
        {
            return MathF.Sqrt(v.X * v.X + v.Y * v.Y);
        }

        //Normalized vector - vector with direction of given and length of 1
        public static Vector2f Normalize(Vector2f v)
        {
            float length = Length(v); 
            if (length == 0)
                return new Vector2f(0, 0);

            return v / length;
        }

        //Rotates vector on a given angle (in degrees)
        public static Vector2f Rotate(Vector2f v, float angle)
        {
            float cos = MathF.Cos(angle / 180 * MathF.PI);
            float sin = MathF.Sin(angle / 180 * MathF.PI);
            float _x = v.X;
            float _y = v.Y;

            return new Vector2f(_x * cos - _y * sin, _x * sin + _y * cos);
        }

        //Middle of line segment limited with given points
        public static Vector2f Middle(Vector2f from, Vector2f to)
        {
            return new Vector2f((to.X + from.X) / 2, (to.Y + from.Y) / 2);
        }

        //Projects one vector onto another
        public static Vector2f Project(Vector2f from, Vector2f to)
        {
            return Dot(from, to) / Length(to) * Normalize(to);
        }
        
        //Returns sign of given float in integer form: 1 for positive and 0, -1 for negative
        //MathF function returns 0 for 0 and it breaks some calculations, so this was made
        public static int Sign(float v)
        {
            return v < 0 ? -1 : 1;
        }
    }
}
