﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Roguefarm
{
    //Manages game physics - movement and collisions
    class Physics
    {
        //Represents pair of objects to be collided
        class ObjectPair
        {
            public PhysicsObject A;
            public PhysicsObject B;

            public ObjectPair()
            {
                A = new PhysicsObject(MathV.Zero, 0, 0);
                B = new PhysicsObject(MathV.Zero, 0, 0);
            }

            public ObjectPair(PhysicsObject a, PhysicsObject b)
            {
                A = a;
                B = b;
            }
            public override bool Equals(Object o)   //makes sure that (A, B) == (B, A)
            {
                if (o is ObjectPair)
                {
                    ObjectPair other = o as ObjectPair;
                    if (A == other.A && B == other.B || A == other.B && B == other.A)
                        return true;
                }
                return false;
            }
            public static bool operator ==(ObjectPair left, ObjectPair right)
            {
                return left.Equals(right);
            }
            public static bool operator !=(ObjectPair left, ObjectPair right)
            {
                return !left.Equals(right);
            }
        }

        //represents pair of object and wall to be collided
        class ObjectWallPair
        {
            public PhysicsObject A;
            public Wall W;

            public ObjectWallPair()
            {
                A = new PhysicsObject(MathV.Zero, 0, 0);
                W = new Wall(0, 0, 0, 0);
            }

            public ObjectWallPair(PhysicsObject a, Wall w)
            {
                A = a;
                W = w;
            }
            public override bool Equals(Object o)
            {
                if (o is ObjectWallPair)
                {
                    ObjectWallPair other = o as ObjectWallPair;
                    if (A == other.A && W == other.W)
                        return true;
                }
                return false;
            }
            public static bool operator ==(ObjectWallPair left, ObjectWallPair right)
            {
                return left.Equals(right);
            }
            public static bool operator !=(ObjectWallPair left, ObjectWallPair right)
            {
                return !left.Equals(right);
            }
        }

        #region Constants
        public static readonly float A = 1;     //Universal acceleration constant
        public static readonly float Mu = .4f;  //Universal frictional constant
        public static readonly float N = 2f;    //Universal interaction constant
        #endregion


        #region Objects
        private static List<PhysicsObject> objects; //reference to room's objects
        private static Room room;   //current room reference

        private static Player player;   //current player reference
        #endregion


        #region debug
        private static int collisions = 0;
        #endregion


        #region Object Management
        public static void Reset() { room = null; }

        public static void SetRoom(Room r) { room = r; }
        #endregion


        #region Updating
        public static void Update(float delta)  //It's where the magic begins
        {
            collisions = 0;     //debug value
            if (room != null)   //Make sure the room is here
            {
                objects = room.Objects;

                ObjectCollisions();
                WallCollisions();
                DoorCollisions();

                foreach (PhysicsObject obj in objects)  //Update physics on each object
                    obj.UpdatePhysics(delta);
            }
        }

        //Detect object to object collisions
        private static void ObjectCollisions()
        {
            //Create list of object pairs
            List<ObjectPair> pairs = new List<ObjectPair>();

            foreach (PhysicsObject obj1 in objects)
            {
                if (obj1 is Player)
                    player = obj1 as Player;    //update player references

                foreach (PhysicsObject obj2 in objects) 
                    if (obj1 != obj2)
                    {
                        ObjectPair newpair = new ObjectPair(obj1, obj2);
                        if (!pairs.Contains(newpair))
                        {
                            pairs.Add(newpair);

                            if (obj1.Collide(obj2))
                            {
                                //Calls to objects' hooks
                                obj1.OnCollision(obj2);
                                obj2.OnCollision(obj1);

                                collisions++;
                            }
                        }
                    }
            }
        }

        //Detect object to wall collisions
        private static void WallCollisions()
        {
            if (room != null)
            {
                foreach (PhysicsObject obj in objects)
                    foreach (Wall wall in room.Walls)
                    {
                        if (wall.Solid && obj.Collide(wall.A, wall.B))
                        {
                            obj.OnCollision(wall);  //Call to object's hook
                            collisions++;
                        }
                    }
            }
        }
       
        //Detect player's interactions with doors
        private static void DoorCollisions()
        {
            //Collision of player with every door in the room
            foreach (Door door in room.Doors)
                if (door != null && player != null)
                    if (door.TryEnter(player))
                    {
                        Program.DoorTriggered(door.Direction);
                        break;
                    }

            //This probably will be replaced with event system and physics engine will only fire the event for map to be handled
        }
        #endregion


        #region debug
        public static string GetDebugInfo()
        {
            if (room != null)
                return String.Format("O: {0} | W: {1} | C: {2}\n",
                                     objects.Count, room.Walls.Count, collisions);
            else
                return "No Room Assigned";
        }
        #endregion
    }
}
