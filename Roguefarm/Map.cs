﻿using System;
using System.Collections.Generic;
using System.Text;
using SFML.System;
using SFML.Graphics;
using System.Windows.Markup;
using System.Runtime.CompilerServices;
using System.Linq;

namespace Roguefarm
{
    //Represents wall connecting 2 points
    class Wall
    {
        public Vector2f A;
        public Vector2f B;
        public bool Solid=true;
        public Wall(Vector2f a, Vector2f b) { A = a; B = b; }
        public Wall(float ax, float ay, float bx, float by) 
        { 
            A = new Vector2f(ax, ay); 
            B = new Vector2f(bx, by); 
        }
    }
   
    //Same wall, but with door features
    class Door : Wall
    {
        private Texture textureOpen = new Texture("Sprites/Selected/door.png");
        private Texture textureClose = new Texture("Sprites/Selected/doorClosed.png");
        private Sprite sprite;

        Vector2f _offset;   //how far player need to get to enter the door

        //Coordinates of door hitbox
        public Vector2f DA { get => A + _offset; }
        public Vector2f DB { get => B + _offset; }

        //Coordinates of point to place player when enters the room through this door
        public Vector2f EnterPoint { get => MathV.Middle(A, B) - _offset; }
        public int Direction { get; set; }

        public bool Open    //Controlls doors passabiility and sprite
        { 
            get => !Solid;
            set 
            { 
                Solid = !value;
                if (value) sprite.Texture = textureOpen;
                else sprite.Texture = textureClose;
            } 
        }
        public Door(Vector2f a, Vector2f b, Vector2f offset, int direction) : base(a, b) 
        {
            _offset = offset;
            Direction = direction;
            SetSprite();
        }
        public Door(float ax, float ay, float bx, float by, float dx, float dy, int direction) : 
            base(ax, ay, bx, by) 
        {
            _offset = new Vector2f(dx, dy);
            Direction = direction;
            SetSprite();
        }

        private void SetSprite()
        {
            sprite = new Sprite(textureOpen);

            sprite.Rotation = 90 * (5 - Direction);

            sprite.Position = DA - _offset;
        }
        
        //Check if player enters the door
        public bool TryEnter(Player p)
        {
            if (p.Collide(DA, DB))
                return true;
            return false;
        }

        public void Draw(RenderWindow w)
        {
            w.Draw(sprite);
        }
    }

    //Room for characters to be in
    class Room
    {
        //Class to represent room initialization parameters
        public class Parameters
        {
            public float width;
            public float height;
            public float indent;
            public float doorwidth;
            public bool[] doorlist;

            public Parameters(float w, float h, float d, float dw, bool[] doors)
            {
                width = w; height = h; indent = d; doorwidth = dw; doorlist = doors;
            }

            public Parameters WithDoors(bool[] doors)
            {
                return new Parameters(width, height, indent, doorwidth, doors);
            }
        }

        #region Drawing
        private Texture texture = new Texture("Sprites/Selected/room2.png");
        private Sprite sprite;
        #endregion


        #region System
        public enum Type
        {
            Undecided, Regular, Start, End
        }

        public Type type;

        private bool cleared = false;
        private int enemyCount = 0;
        private Random rd;
        #endregion


        #region Walls And Doors
        public List<Wall> Walls { get; } = new List<Wall>();
        public List<Door> Doors { get; } = new List<Door>();

        //Center of the room
        private Vector2f mid;
        public Vector2f Middle { get => mid; }
        #endregion


        #region Objects
        private List<SpriteObject> drawables = new List<SpriteObject>();
        public List<PhysicsObject> Objects { get; } = new List<PhysicsObject>();

        //Since objects can be added and deleted in the midle of update
        //  it can invalidate iterators. This prevents such problems:
        //      objects added and deleted only after update finished
        private Queue<SpriteObject> deleteQueue = new Queue<SpriteObject>();
        private Queue<SpriteObject> addQueue = new Queue<SpriteObject>();
        #endregion


        #region System
        public Room(Parameters p, Type t, Random random)
        {
            rd = random;

            mid = new Vector2f(p.width / 2, p.height / 2);

            SetUpWalls(p.width, p.height, p.indent, p.doorwidth, p.doorlist);
            type = t;

            if (type == Type.Start) cleared = true;

            sprite = new Sprite(texture);

            SpawnEnemies();
        }
        #endregion


        #region Walls And Doors
        //Create walls in room according to parameters
        private void SetUpWalls(float w, float h, float d, float dw, bool[] doors)
        {
            //Lookup table constructed according to parameters, used to create walls and doors
            Vector2f[] points = new Vector2f[] {new Vector2f(d, d), new Vector2f(d, (h - dw) / 2), 
                                                new Vector2f(d, (h + dw) / 2), new Vector2f(d, h - d),
            
                                                new Vector2f(d, h - d), new Vector2f((w - dw)/2, h - d),
                                                new Vector2f((w + dw)/2, h - d), new Vector2f(w - d, h - d),
                                                
                                                new Vector2f(w - d, h - d), new Vector2f(w - d, (h + dw) / 2),
                                                new Vector2f(w - d, (h - dw) / 2), new Vector2f(w - d, d),

                                                new Vector2f(w - d, d), new Vector2f((w + dw) / 2, d),
                                                new Vector2f((w - dw) / 2, d), new Vector2f(d, d)};

            Vector2f[] offsets = new Vector2f[] { new Vector2f(-50, 0), new Vector2f(0, 50), 
                                                  new Vector2f(50, 0), new Vector2f(0, -50) };

            for (int i = 0; i < 4; i++) //Walls generation loop
            {
                if (doors != null && doors[i])
                {   //if wall has door, it's onstracted out of three segments -
                    //  two walls and one door
                    Walls.Add(new Wall(points[4 * i], points[4 * i + 1]));
                    Walls.Add(new Wall(points[4 * i + 2], points[4 * i + 3]));

                    Door newdoor = new Door(points[4 * i + 1], points[4 * i + 2], offsets[i], i) { Open = true};
                    Walls.Add(newdoor);
                    Doors.Add(newdoor);
                }
                else
                {   //otherwise - only one segment and no door
                    Walls.Add(new Wall(points[4 * i], points[4 * i + 3]));
                    Doors.Add(null);
                }
            }
        }
        public void AddWall(Wall w)
        {
            Walls.Add(w);
        }

        //Open and close doors
        public void ToggleDoor(int d, bool open)
        {
            if (Doors[d] != null)
                Doors[d].Open = open;
        }
        public void ToggleDoor(int d)
        {
            if (Doors[d] != null)
                Doors[d].Open = !Doors[d].Open;
        }
        public void ToggleDoors(bool open)
        {
            foreach (Door door in Doors)
                if (door != null)
                    door.Open = open;
        }
        #endregion


        #region Objects
        private void AddObject(SpriteObject c)
        {
            if (c is Enemy)
            {
                enemyCount++;
                cleared = false;
            }

            ToggleDoors(enemyCount == 0);

            Objects.Add(c);
            drawables.Add(c);
        }
        private void RemoveObject(SpriteObject c)
        {
            if (c is Player)
                Program.NoPlayer();

            if (c is Enemy)
            {
                enemyCount--;

                if ((c as Enemy).Boss)
                    Program.Win();
            }

            ToggleDoors(enemyCount == 0);

            Objects.Remove(c);
            drawables.Remove(c);
        }

        public void MarkForAdd(SpriteObject c)
        {
            addQueue.Enqueue(c);
        }
        public void MarkForDelete(SpriteObject c)
        {
            deleteQueue.Enqueue(c);
        }
        public void Update(float delta)
        {
            //Update objects
            foreach (PhysicsObject c in Objects)
                c.Update(delta);

            //Then delete and add objects scheduled during update
            while (deleteQueue.Count > 0)
                RemoveObject(deleteQueue.Dequeue());

            while (addQueue.Count > 0)
                AddObject(addQueue.Dequeue());
        }
        public void EnterRoom(int door, Player player)
        {
            Objects.Add(player);
            drawables.Add(player);

            if (door != -1)
                player.Position = Doors[door].EnterPoint;

            if (!cleared)   //make enemies aim player
            {
                int count = 0;
                foreach (PhysicsObject o in drawables)
                    if (o is Enemy)
                    {
                        (o as Enemy).Aim = player;
                        count++;
                    }

                if (count > 0) ToggleDoors(false);
            }
                
        }
        public void LeaveRoom(Player player)
        {
            Objects.Remove(player);
            drawables.Remove(player);
        }

        private void SpawnEnemies()
        {
            if (type == Type.Regular)
            {
                for (int i = 0; i < rd.Next(10); i++)   //random count
                {
                    float size = (float)rd.NextDouble() + .5f;  //random size
                    //random position
                    Vector2f position = MathV.Normalize(new Vector2f((float)rd.NextDouble(), (float)rd.NextDouble()));

                    Enemy e = new Enemy(mid + position * 50, size);

                    AddObject(e);
                }
            }
            else if (type == Type.End)
            {
                AddObject(new Enemy(mid, true));
            }
        }
        #endregion


        #region Drawing
        public void Draw(RenderWindow w, bool debug)
        {
            w.Draw(sprite);

            foreach (Door d in Doors)
                if (d != null) d.Draw(w);

            foreach (SpriteObject c in drawables)
            {
                c.Draw(w);
                if (debug) c.DrawHitbox(w);
            }
        }
        #endregion

    }

    //Generates and contains rooms
    class Map
    {
        //Current player reference
        private Player player = null;


        #region Rooms
        private Dictionary<Vector2i, Room> rooms = new Dictionary<Vector2i, Room>();    //The rooms themselves
        private Vector2i current = new Vector2i(0, 0);  //Coordinates of current room
        public Room CurrentRoom { get => rooms[current]; }
        #endregion


        #region Generation
        private int _seed = new Random().Next();    //Random seed for random
        private Random rd;

        //Lookup table for room dirrectional offsets
        private Vector2i[] offsets = new Vector2i[] { new Vector2i(-1, 0), new Vector2i(0, +1), 
                                                      new Vector2i(+1, 0), new Vector2i(0, -1)};
        #endregion


        #region debug
        //Number of generation attempts
        private int attempts = 1;
        #endregion


        public Map(int radius, Room.Parameters ps)
        {
            //Random initialization
            rd = new Random(_seed);

            Generate(radius, ps);
        }
        public Map(int radius, Room.Parameters ps, int seed)
        {
            //Random initialization
            _seed = seed;
            rd = new Random(_seed);

            Generate(radius, ps);
        }


        #region Generation
        private void Generate(int radius, Room.Parameters ps)
        {
            //Tilemap for new rooms' types
            Dictionary<Vector2i, Room.Type> rs = new Dictionary<Vector2i, Room.Type>();

            //First pass - generating layout - iterates until generated enough rooms
            List<Vector2i> endRooms;
            while (!GenerateLayout(rs, radius, out endRooms))   //Retry if not enough rooms
            {
                rs.Clear();
                attempts++;
            }

            //Second pass - defining room types
            SetTypes(rs, endRooms);

            //Third pass - creating room objects;
            CreateRooms(rs, ps);

            //Set room for physics engine
            Physics.SetRoom(CurrentRoom);
        }

        private bool GenerateLayout(Dictionary<Vector2i, Room.Type> rs, int radius, out List<Vector2i> endRooms)
        {
            int maxCount = radius * radius + rd.Next(-2, 3);    //Maximal room count
            int count = 1;  //Current count

            int restarts = 0;  //Count of times starting room was re-added to generation queue 

            rs.Add(new Vector2i(0, 0), Room.Type.Undecided);    //First room

            endRooms = new List<Vector2i>();    //List of end candidates

            Queue<Vector2i> toGenerate = new Queue<Vector2i>(); //Generation queue
            toGenerate.Enqueue(new Vector2i(0, 0));     //Adding start to queue

            while (toGenerate.Count > 0)    //Generate until queue is empty
            {
                Vector2i coords = toGenerate.Dequeue();     //Current generation point
                bool placed = false;

                for (int i = 0; i < 4; i++)
                {
                    if (count >= maxCount)      //No more room needed  //While loop will still continue
                        break;                                        //but for loop will fail to continue past this
                                                                     //so every room left in generation queue
                                                                    //will end up in end candidates list
                                                                   //since no room was generated from them

                    Vector2i newCoords = coords + offsets[i];   //Adding offsets to try generate room in every cardinal direction

                    if (Math.Abs(newCoords.X) > radius || Math.Abs(newCoords.Y) > radius)   //Out of bounds
                        continue;

                    if (rs.TryGetValue(newCoords, out _))   //Already generated
                        continue;

                    if (CheckNear(rs, newCoords) > 1)   //Avoiding corridor loop
                        continue;

                    if (rd.NextDouble() > .5)   //Chance
                        continue;

                    rs.Add(newCoords, Room.Type.Undecided);     //Add new room to tilemap
                    toGenerate.Enqueue(newCoords);  //And to generation queue
                    placed = true;
                    count++;
                }

                if (!placed)    //Mark as end candidate if no room was generated 
                    endRooms.Add(coords); 

                if (toGenerate.Count == 0 && count < maxCount && restarts < 4) //Re-add start room to generation list
                {
                    restarts++; //Avoid endless loop. If there's still not enough room, layout generation will be restarted
                    toGenerate.Enqueue(new Vector2i(0, 0));
                }
            }

            if (count < maxCount)   //Unable to generate desired amount of rooms
                return false;

            return true;    //Layout generated succesfully
        }
    
        private void SetTypes(Dictionary<Vector2i, Room.Type> rs, List<Vector2i> endRooms)
        {
            //Starting in the middle
            rs[new Vector2i(0, 0)] = Room.Type.Start;

            rs[endRooms.Last()] = Room.Type.End;

            //Every other room is regular for now
            foreach (var room in new Dictionary<Vector2i, Room.Type>(rs))
                if (room.Value == Room.Type.Undecided)
                    rs[room.Key] = Room.Type.Regular;
        }

        private void CreateRooms(Dictionary<Vector2i, Room.Type> rs, Room.Parameters ps)
        {
            //Room objects generation
            foreach (var room in rs)    
            {
                bool[] doorset = new bool[4];   //Set of doors of current room
                for (int i = 0; i < 4; i++)
                {
                    doorset[i] = rs.TryGetValue(room.Key + offsets[i], out _);  //Check every cardinal direction
                }

                rooms.Add(room.Key, new Room(ps.WithDoors(doorset), room.Value, rd));   //Add rooom to room list
            }
        }

        private int CheckNear(Dictionary<Vector2i, Room.Type> rs, Vector2i coords)
        {
            //count amount of rooms adjacent to given
            int count = 0;
            for (int i = 0; i < 4; i++)
                if (rs.TryGetValue(coords + offsets[i], out _))     //check in every cardinal direction
                    count++;

            return count;
        }
        #endregion


        #region Player Control
        public void SetPlayer(Player pl)
        {
            player = pl;
            CurrentRoom.EnterRoom(-1, player);
            player.PlaceAt(CurrentRoom.Middle);
        }

        public void ResetPlayer()
        {
            CurrentRoom.LeaveRoom(player);
            player = null;
        }

        public bool Move(int dir)
        {
            //Traverse from current room to the given direction
            if (rooms.TryGetValue(current + offsets[dir], out _))
            {
                CurrentRoom.LeaveRoom(player);

                current += offsets[dir];

                CurrentRoom.EnterRoom(EnterDir(dir), player);
                Physics.SetRoom(CurrentRoom);

                return true;
            }
            else
                return false;   //Unsuccessful
        }

        private int EnterDir(int dir)
        {
            //Calculates direction opposite to given
            //Relies on the next map of directions
            //   3
            // 0   2
            //   1
            if (dir < 2)
                return dir + 2;
            else
                return dir - 2;
        }
        #endregion


        #region debug
        public string GetDebugInfo()
        {
            string roomtype;
            switch (CurrentRoom.type)
            {
                case Room.Type.Start:
                    roomtype = "Start";
                    break;
                case Room.Type.Regular:
                    roomtype = "Regular";
                    break;
                case Room.Type.End:
                    roomtype = "End";
                    break;
                default:
                    roomtype = "Unknown";
                    break;
            }

            return string.Format("{1} | {3} | {4}, {5} | {2}\n{0}\n",
                                 _seed, attempts, roomtype,
                                 Convert.ToString(rooms.Count).PadLeft(2),
                                 Convert.ToString(current.X).PadLeft(2),
                                 Convert.ToString(current.Y).PadLeft(2));
        }
        #endregion
    }
}
