﻿using System;
using System.Collections.Generic;
using System.Text;
using SFML.System;
using SFML.Graphics;

namespace Roguefarm
{
    class Projectile : SpriteObject
    {
        private float _pushforce;
        private Vector2f _direction;

        public float Damage { get => _sender.Damage; }

        private Character _sender;
        public Character Sender { get => _sender; }

        public Projectile(Vector2f position, float mass, HitboxParam param,
            float pushforce, Vector2f direction, Character sender) : base(position, mass, param)
        {
            _terminal_velocity = 15;

            _pushforce = pushforce;
            _direction = direction;
            _sender = sender;
        }

        public override void Update(float delta)
        {
            _force += _direction * _pushforce;

            base.Update(delta);
        }

        public override void OnCollision(PhysicsObject other)
        {
            if (other is Character)
            {
                Character c = other as Character;
                if (c != _sender)
                {
                    _sender.Attack(c);
                    Program.MarkForDelete(this);
                }
            }
        }

        public override void OnCollision(Wall wall)
        {
            Program.MarkForDelete(this);
        }
    }

    class PlayerProjectile : Projectile
    {
        private Texture texture = new Texture("Sprites/Selected/spell.png");
        public PlayerProjectile(Vector2f position, Vector2f direction, Player player) : 
            base(position, .4f, new HitboxParam { rad = 25 * player.Damage / Player.basicDMG, color = new Color(152, 3, 252), thickness = 5},
                3, direction, player)
        {
            Sprite = new Sprite(texture) { Scale = new Vector2f(1, 1) * player.Damage / Player.basicDMG};
            _terminal_velocity *= Player.basicDMG / player.Damage;
        }
    }
}
