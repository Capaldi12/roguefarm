﻿using System;
using SFML.System;
using SFML.Graphics;

namespace Roguefarm
{
    class Character : SpriteObject
    {
        protected float _maxhealth;
        public float MaxHealth { get => _maxhealth; set => _maxhealth = value; }
        
        protected float _health;
        public float Health { get => _health; }


        protected float _damage;
        public float Damage { get => _damage; set => _damage = value; }

        protected float _defense;
        public float Defense { get => _defense; set => _defense = value; }
  

        protected float _acceleration;
        public float Acceleration { get => _acceleration; set => _acceleration = value; }

        private float _damageCooldown;
        public float DamageCooldown { get => _damageCooldown; set => _damageCooldown = value; }

        private float cooldownTime = 0;

        
        public Character(Vector2f pos, float mass, HitboxParam param, 
                         float health, float damage, float defense, float acceleration): 
            base(pos, mass, param)
        {
            _maxhealth = health;
            _health = health;

            _damage = damage;
            _defense = defense;
            _acceleration = acceleration;
            _damageCooldown = 10;
        }

        public Character(Vector2f pos, float mass, HitboxParam param) : base(pos, mass, param)
        {

            _maxhealth = 100;
            _health = 100;

            _damage = 10;
            _defense = 0;
            _acceleration = 1f;
            _damageCooldown = 10;
        }

        //Damage and health
        public float Hurt(float v)
        {
            if (cooldownTime < 0)
            {
                cooldownTime = _damageCooldown;

                v *= 1 - _defense;
                _health -= v;
                if (_health <= 0)
                    Die();
            }

            
            return _health;
        }
        private void Die()
        {
            Program.MarkForDelete(this);
        }
        public float Heal(float v)
        {
            _health += v;
            if (_health > _maxhealth)
                _health = _maxhealth;

            return _health;
        }
        public void Attack(Character other)
        {
            other.Hurt(_damage);
        }

        //Makes character move to something or in specific direcion
        public void MoveTowards(Vector2f newPos)
        {
            AddForce(MathV.Normalize(newPos - Position) * _acceleration);
        }
        public void MoveIn(Vector2f direction)
        {
            AddForce(MathV.Normalize(direction) * _acceleration);
        }

        public override void Update(float delta)
        {
            cooldownTime -= delta;

            base.Update(delta);
        }

        public override void OnCollision(PhysicsObject other)
        {
            if (other is Projectile)
            {
                Projectile p = other as Projectile;
                if (p.Sender != this)
                    AddForce(MathV.Normalize(Position - p.Position) * Physics.N * p.Damage);

            }
            else
                base.OnCollision(other);
        }
    }
}
