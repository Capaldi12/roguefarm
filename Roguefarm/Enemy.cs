﻿using System;
using System.Collections.Generic;
using SFML.System;
using SFML.Graphics;

namespace Roguefarm
{
    class Enemy : Character
    {
        Texture texture = new Texture("Sprites/Selected/enemy.png");

        public static float basicRad = 50;
        public static float basicThick = 5;
        public static Color basicColor = new Color(100, 10, 20);
        public static float basicMass = 1.2f;
        public static float basicHP = 100;
        public static float basicDMG = 10f;
        public static float basicDEF = 0;
        public static float basicAcc = 1;
        public static float basicMaxSpeed = 8;

        public static float boosSize = 3;

        private bool _boss = false;
        public bool Boss { get => _boss; }
        
        private Character _aim = null;

        //Character that enemy will try to attack
        public Character Aim { get => _aim; set => _aim = value; }

        public Enemy(Vector2f pos) :
            base(pos, basicMass, new HitboxParam() { rad = basicRad, color = basicColor, thickness = basicThick }, 
                basicHP, basicDMG, basicDEF, basicAcc)
        {
            _terminal_velocity = basicMaxSpeed;
            setSprite(1);
        }

        //Different-sized enemies
        public Enemy(Vector2f pos, float size) :
            base(pos, basicMass * size, new HitboxParam() { rad = basicRad * size, color = basicColor, thickness = basicThick }, 
                basicHP, basicDMG, basicDEF, basicAcc)
        {
            _terminal_velocity = basicMaxSpeed;
            setSprite(size);
        }

        //Enemy - Boss
        public Enemy(Vector2f pos, bool boss) :
            base(pos, basicMass * boosSize, new HitboxParam() { rad = basicRad * boosSize, color = basicColor, thickness = basicThick },
                basicHP * boosSize * 2, basicDMG * boosSize / 2, basicDEF, basicAcc * boosSize)
        {
            _terminal_velocity = basicMaxSpeed;
            setSprite(boosSize);
            _boss = boss;
        }

        private void setSprite(float size)
        {
            Sprite = new Sprite(texture) { Scale = new Vector2f(size * .5f, size * .5f) };
        }


        public override void Update(float delta)
        {
            if (_aim != null)
            {
                if (!Collide(_aim))
                    MoveTowards(_aim.Position);
                else
                    Attack(_aim);
            }

            base.Update(delta);
        }
    }
}
